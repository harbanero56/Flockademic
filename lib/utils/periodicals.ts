import { Periodical, PublishedPeriodical } from '../interfaces/Periodical';
import { Session } from '../interfaces/Session';

export function isPublic(periodical: Partial<Periodical>): periodical is PublishedPeriodical {
  return (
    typeof periodical.datePublished !== 'undefined'
    && typeof periodical.creator !== 'undefined'
    && typeof periodical.name === 'string'
  );
}

export function isPeriodicalOwner(periodical: { creator: { identifier: string } }, session: Session): boolean {
  return (
    typeof session.account !== 'undefined' &&
    periodical.creator.identifier === session.account.identifier
  );
}

export function isArticleAuthor(article: { author: Array<{ identifier?: string }>; }, session: Session): boolean {
  if (typeof session.account === 'undefined') {
    return false;
  }

  const account = session.account;

  return article.author.filter((author) => author.identifier && author.identifier === account.identifier).length === 1;
}
