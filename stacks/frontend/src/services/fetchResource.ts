// The main goal of this module is to have a single place to add potential security measures.
// Furthermore, it provides a single place to insert the API's base path.
// That said, we should try to keep this module's scope limited to prevent tight coupling.
// (Coincidentally, it's also easier to mock in tests than `fetch`.)

export const fetchResource = (resource: string, init?: RequestInit) => {
    return fetch(`${process.env.API_URL}${resource}`, init);
};
