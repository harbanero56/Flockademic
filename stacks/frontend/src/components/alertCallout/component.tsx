require('./styles.scss');

import * as React from 'react';
import { connect } from 'react-redux';
import { Transition } from 'react-transition-group';

import { AlertState } from '../../ducks/alert';
import { AppState } from '../../ducks/app';

interface AlertCalloutState {
  lastError?: string | JSX.Element;
  lastWarning?: string | JSX.Element;
  lastSuccess?: string | JSX.Element;
}
export interface AlertCalloutStateProps {
  alerts: AlertState;
}
export interface AlertCalloutOwnProps {}

export type AlertCalloutProps = AlertCalloutOwnProps & AlertCalloutStateProps;

export class BareAlertCallout extends React.Component <AlertCalloutProps, AlertCalloutState> {
  constructor(props: AlertCalloutProps) {
    super(props);

    this.state = {};

    this.renderError = this.renderError.bind(this);
    this.renderWarning = this.renderWarning.bind(this);
    this.renderSuccess = this.renderSuccess.bind(this);
    this.cacheError = this.cacheError.bind(this);
    this.cacheWarning = this.cacheWarning.bind(this);
    this.cacheSuccess = this.cacheSuccess.bind(this);
  }

  public render() {
    return (
      <div className="alerts container">

        <Transition
          in={!!this.props.alerts.error}
          onEnter={this.cacheError}
          unmountOnExit={true}
          timeout={200}
          className="errorTransition"
        >
          {this.renderError}
        </Transition>
        <Transition
          in={!!this.props.alerts.warning && !this.props.alerts.error}
          onEnter={this.cacheWarning}
          unmountOnExit={true}
          timeout={200}
          className="warningTransition"
        >
          {this.renderWarning}
        </Transition>
        <Transition
          in={!!this.props.alerts.success && !this.props.alerts.error && !this.props.alerts.warning}
          onEnter={this.cacheSuccess}
          unmountOnExit={true}
          timeout={200}
          className="successTransition"
        >
          {this.renderSuccess}
        </Transition>
      </div>
    );
  }

  // This is necessary to make the alert linger while it fades out:
  private cacheError() {
    /* istanbul ignore if: shouldn't happen since it's called in Transition's onEnter prop */
    if (!this.props.alerts.error) {
      return;
    }

    return this.setState({ lastError: this.props.alerts.error });
  }

  // This is necessary to make the alert linger while it fades out:
  private cacheWarning() {
    /* istanbul ignore if: shouldn't happen since it's called in Transition's onEnter prop */
    if (!this.props.alerts.warning) {
      return;
    }

    return this.setState({ lastWarning: this.props.alerts.warning });
  }

  // This is necessary to make the alert linger while it fades out:
  private cacheSuccess() {
    /* istanbul ignore if: shouldn't happen since it's called in Transition's onEnter prop */
    if (!this.props.alerts.success) {
      return;
    }

    return this.setState({ lastSuccess: this.props.alerts.success });
  }

  private renderError(state: string) {
    return (
      <div role="alert" className={`message is-danger ${state}`}>
        <div className="message-body">
          {this.props.alerts.error || this.state.lastError}
        </div>
      </div>
    );
  }

  private renderWarning(state: string) {
    return (
      <div role="alert" className={`message is-warning ${state}`}>
        <div className="message-body">
          {this.props.alerts.warning || this.state.lastWarning}
        </div>
      </div>
    );
  }

  private renderSuccess(state: string) {
    return (
      <div role="alert" className={`message is-success ${state}`}>
        <div className="message-body">
          {this.props.alerts.success || this.state.lastSuccess}
        </div>
      </div>
    );
  }
}

// The next function is trivial (and forced to be correct through the types),
// but testing it requires inspecting the props of the connected component and mocking the store.
// Figuring that out is not worth it, so we just don't test this:
/* istanbul ignore next */
function mapStateToAlertCalloutProps(state: AppState): AlertCalloutStateProps {
  return { alerts: state.alert };
}

export const AlertCallout = connect<AlertCalloutStateProps, {}, AlertCalloutOwnProps, AppState>(
  mapStateToAlertCalloutProps,
)(BareAlertCallout);
