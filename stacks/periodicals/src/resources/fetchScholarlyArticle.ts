import { GetScholarlyArticleResponse } from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { fetchScholarlyArticle as commandHandler } from '../services/scholarlyArticle';

export async function fetchScholarlyArticle(
  context: Request<undefined> & DbContext & SessionContext,
): Promise<GetScholarlyArticleResponse> {
  if (!context.params || context.params.length < 2) {
    throw(new Error('Could not find an article without an article ID.'));
  }

  const session = (context.session instanceof Error) ? undefined : context.session;

  try {
    const article = await commandHandler(context.database, context.params[1], session);

    if (article === null) {
      return {};
    }

    // The command handler might return private data (such as session keys of the creator),
    // so only return required values.
    return {
      associatedMedia: article.associatedMedia,
      author: article.author,
      datePublished: article.datePublished,
      description: article.description,
      identifier: article.identifier,
      isPartOf: article.isPartOf,
      name: article.name,
    };
  } catch (e) {
      throw new Error(`Could not find an article with ID \`${context.params[1]}\`.`);
  }
}
