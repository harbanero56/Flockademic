import { spawn } from 'child_process';
import { readdirSync, statSync } from 'fs';
import * as path from 'path';

// body-parser is listed as a devDependency, so that's good enough:
// tslint:disable-next-line:no-implicit-dependencies
import * as bodyParser from 'body-parser';
// express is listed as a devDependency, so that's good enough:
// tslint:disable-next-line:no-implicit-dependencies
import * as express from 'express';

const app: express.Application = express();
app.use(bodyParser.text({ type: () => true }));

if (process.env.NODE_ENV !== 'production') {
  app.use('/upload*', (req, res) => {
    if (req.method === 'GET') {
      // tslint:disable-next-line:no-console
      console.log('Mocknig download at', req.url);
      res.set('Content-Disposition', 'attachment;filename=mock_file.txt');
      res.send('Mock file');
    }

    if (req.method === 'PUT') {
      // tslint:disable-next-line:no-console
      console.log('Mocking successful file upload at', req.url, '...');
    }

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    setTimeout(() => res.send(), 500);
  });
}

const lambdas =
  readdirSync('../stacks')
  .filter((name) => {
    if (name === 'frontend') {
      return false;
    }

    return (statSync(path.join('../stacks', name)).isDirectory());
  });

lambdas.forEach(proxyRequestsToLambda);

function proxyRequestsToLambda(lambda: string) {
  app.all(`/${lambda}(/*)?`, (req, res) => {
    const event = {
      // For some reason body-parser sets this to an empty object for GET requests, so turn that into the empty string:
      body: (typeof req.body !== 'string') ? '' : req.body,
      headers: req.headers,
      httpMethod: req.method,
      path: req.path,
      // This assumes that every Lambda expects to be passed the tail of the URL as `tail`,
      // rather than parsing the path in API gateway and passing on the individual parts:
      pathParameters: (Object.keys(req.params).length === 2) ? { tail: req.params[1] } : null,
      queryStringParameters: req.query,
    };

    // tslint:disable-next-line:no-empty
    const noop = () => {};
    const context = {
      awsRequestId: 'arbitrary',
      callbackWaitsForEmptyEventLoop: false,
      done: noop,
      fail: noop,
      functionName: lambda,
      functionVersion: 'arbitrary',
      getRemainingTimeInMillis: () => 1337,
      invokedFunctionArn: 'arbitrary',
      logGroupName: 'arbitrary',
      logStreamName: 'arbitrary',
      memoryLimitInMB: 42,
      succeed: noop,
    };

    const responseCallback = (errors: any, response: { body: string, headers: object, statusCode: string }) => {
      res.header('Access-Control-Allow-Origin', 'http://localhost:8000');
      if (process.env.frontend_url) {
        res.header('Access-Control-Allow-Origin', process.env.frontend_url);
      }
      if (typeof errors !== 'undefined') {
        res.send('Error');
      } else {
        res.set(response.headers);
        res.status(parseInt(response.statusCode, 10));
        res.send(response.body);
      }
    };

    callLambda(lambda, event, context, responseCallback);
  });

  // tslint:disable-next-line:no-console
  console.log(`Resource initialised: /${lambda}/*`);
}

function callLambda(lambda: string, ...lambdaArgs: any[]) {
  const modulePath = `../stacks/${lambda}/dist/stacks/${lambda}/src/${lambda}_lambda`;

  const lambdaModule = require(modulePath);

  if (typeof(lambdaModule.handler) !== 'function') {
  // tslint:disable-next-line:no-console
    console.error(`Lambda ${lambda} does not export a handler function`);

    return;
  }

  return lambdaModule.handler(...lambdaArgs);
}

const server = app.listen(process.env.PORT || 3000, () => {
  const address = server.address();

  // server.address() will only be a string when listening on a pipe or UNIX domain socket:
  // https://nodejs.org/api/net.html#net_server_address
  // In other words, the following will be `false` and thus never return.
  // It does allow TypeScript to correctly infer the type of address, however.
  if (typeof address === 'string') {
    return;
  }

  if (process.env.NODE_ENV !== 'production') {
    // tslint:disable-next-line:no-console
    console.log(`API running on port ${address.port}, now starting the front-end:`);

    const frontendProcess = spawn(
      'yarn',
      [ 'run', 'server', '--', '--color', `--env.api_url=http://localhost:${address.port}` ],
      { cwd: '../stacks/frontend' },
    );

    frontendProcess.stdout.pipe(process.stdout);
    frontendProcess.stderr.pipe(process.stderr);
  } else {
    // tslint:disable-next-line:no-console
    console.log(`API running on port ${address.port}.`);
  }
});
